const inventory = require('./db.cjs')

function problem3() {
  const sortAlphabetically = [...inventory];
  for (let i = 0; i < sortAlphabetically.length - 1; i++) {
    let minIndex = i;
    for (let j = i + 1; j < sortAlphabetically.length; j++) {
      if (
        sortAlphabetically[j]["car_model"][0] <
        sortAlphabetically[minIndex]["car_model"][0]
      )
        minIndex = j;
    }
    let temp = sortAlphabetically[minIndex];
    sortAlphabetically[minIndex] = sortAlphabetically[i];
    sortAlphabetically[i] = temp;
  }
  console.log(sortAlphabetically);
  return;
}

module.exports = problem3;
