const inventory = require('./db.cjs')
function problem2() {
  let lastCarIndex = 0;
  let lastCar = inventory[lastCarIndex]["id"];
  for (let index = 1; index < inventory.length; index++) {
    if (lastCar < inventory[index]["id"]) {
      lastCar = inventory[index]["id"];
      lastCarIndex = index;
    }
  }
  console.log(
    `Last car is a ${inventory[lastCarIndex]["car_make"]} ${inventory[lastCarIndex]["car_model"]}`
  );
  return;
}

module.exports = problem2;
